Feature: Earnings Display
  This set of tests will verify that the childs earnings show up accurately

  Scenario Outline: Empty earnings dashboards
    Given i am logged in as a parent
     When i open the earnings dashboard
     Then i see an empty <amount> dollar balance dashboard
    
    Examples:
      | amount |
      | 0      | 
     
  Scenario Outline: Base earnings dashboard single child
    Given i am logged in as a parent
      And my child has earned <amount> dollars
      And i look at the earnings of a single child
     Then i expect to see <amount> dollars on the dashboard
     
     Examples: 
       | amount |
       | 5      |
